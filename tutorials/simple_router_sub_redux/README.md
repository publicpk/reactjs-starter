This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## React Router
- For tutorials, see https://reacttraining.com/react-router/web/example/basic

## React Redux
- For tutorials, see http://redux.js.org/docs/introduction/Examples.html
- and here too. https://github.com/reactjs/redux/tree/master/examples

## React Router with Redux
- See https://github.com/reactjs/react-router-redux/tree/master/examples
- See https://github.com/reacttraining/react-router/tree/master/packages/react-router-redux
