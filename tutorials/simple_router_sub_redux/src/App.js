import React from 'react';

import { Switch, Route, Link } from 'react-router-dom';
import { ConnectedRouter as Router, routerReducer, routerMiddleware, push } from 'react-router-redux'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

import createHistory from 'history/createBrowserHistory'

import Articles from './Articles'
import './App.css'

const history = createHistory();
const middleware = routerMiddleware(history);
const reducers = () => {

};
const store = createStore(
    combineReducers({
    ...reducers,
    router: routerReducer
    }),
    applyMiddleware(middleware)
);

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

const App = () => (
    <Provider store={store}>
        <Router history={history}>
            <div>
                {/* Navi header */}
                <ul className="router-nav">
                    <li><Link to="/">Home |</Link></li>
                    <li><Link to="/articles">| Articles |</Link></li>
                    <li><Link to="/about">| About</Link></li>
                </ul>
                <hr/>
                {/* Router */}
                <Routes/>
            </div>
        </Router>
    </Provider>
);

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/articles" component={Articles} />
        <Route path="/about" component={About} />
        {/* default handler */}
        <Route component={NotFound} />
    </Switch>
);

const Home = () => (<h1>Home</h1>);
const NotFound = () => (<h1>Not Found</h1>);
const About = () => (<h1>About</h1>);

export default App;
