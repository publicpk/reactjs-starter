This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Quick Overview
```
npm install -g yarn
npm install -g create-react-app
create-react-app my-app
cd my-app
# start app
yarn start

```

## Production Build
```
yarn build
# run build output
yarn global add serve
serve -s build

```
