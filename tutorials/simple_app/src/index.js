import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
const ROOT_NODE = document.getElementById('root');

const App = () => (
    <h1> Hello </h1>
);

ReactDOM.render(
    <App />,
    ROOT_NODE
);
