import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import Articles from './Articles'
import './App.css'

const fakeAuth = (() => {
    var isAuthenticated = false;
    return {
        get: () => isAuthenticated,
        fakeLogin: () => {
            return new Promise((resolve) => {
                setTimeout(()=> {
                    isAuthenticated = true;
                    resolve(isAuthenticated);
                }, 300);
            })
        },
        fakeLogout: () => {
            return new Promise((resolve) => {
                setTimeout(()=> {
                    isAuthenticated = false;
                    resolve(isAuthenticated);
                }, 300);
            })
        }
    };
})();

const App = () => (
    <Router>
        <div>
            {/* Navi header */}
            <ul className="router-nav">
                <li><Link to="/">Home |</Link></li>
                <li><Link to="/articles">| Articles |</Link></li>
                <li><Link to="/about">| About |</Link></li>
                <li><AuthLink authState={fakeAuth.get()}/></li>
            </ul>
            <hr/>
            {/* Router */}
            <Routes/>
        </div>
    </Router>
);

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <AuthRoute path="/articles" component={Articles} />
        <Route path="/about" component={About} />
        <Route path="/login" component={Login} />
        {/* default handler */}
        <Route component={NotFound} />
    </Switch>
);

const Home = () => (<h1>Home</h1>);
const NotFound = () => (<h1>Not Found</h1>);
const About = () => (<h1>About</h1>);

const AuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render = { props => (
        fakeAuth.get() ?
            (<Component {...props}/>) :
            (<Redirect to={{ pathname: '/login',  state: { from: props.location } }}/>)
    )}/>
);

class AuthLink extends React.Component {
    state = {
        authState: false
    };

    fakeLogout = () => {
        fakeAuth.fakeLogout()
            .then((result) => {
                console.log(">>> LOGOUT: " + fakeAuth.get());
                this.setState({authState: false});
            });
    };
    render() {
        // TODO: use this.props.authState instead of fakeAuth.get()
        const authState = fakeAuth.get();
        console.log(">>> AuthLink: " + authState);
        return (authState ?
                <Link to="" onClick={this.fakeLogout}>| Logout</Link> :
                <Link to="/login">| Login</Link>
        )
    }
}

class Login extends React.Component {
    state = {
        redirectToReferrer: false
    };

    fakeLogin = () => {
        fakeAuth.fakeLogin()
            .then((result) => {
                console.log(">>> LOGIN: " + fakeAuth.get());
                this.setState({redirectToReferrer: true});
            });
    };

    render() {
        const { from } = this.props.location.state || { from: { pathname: '/'} };
        const { redirectToReferrer } = this.state;

        if ( redirectToReferrer ) {
            return (<Redirect to={from}/>);
        }
        return (
            <div>
                <p>You need to login.</p>
                <button onClick={this.fakeLogin}>Log in</button>
            </div>
        )
    }
}

export default App;