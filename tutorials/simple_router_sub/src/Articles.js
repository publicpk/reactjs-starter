/**
 * Created by peterk on 7/6/17.
 */
import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';

const Articles = ({ match }) => (
    <div>
        <h1>Articles</h1>
        <ul>
            <li><Link to={`${match.url}/article-01`}> Article 01 </Link></li>
            <li><Link to={`${match.url}/article-02`}> Article 02 </Link></li>
            <li><Link to={`${match.url}/article-03`}> Article 03 </Link></li>
        </ul>
        <ArticlesRouters match={match}/>
    </div>
);

const ArticlesRouters = ({ match }) => (
    <Switch>
        <Route path={`${match.url}/:articleId`} component={Article}/>
        <Route exact path={match.url} render = {() => (
           <h3>Please select an article.</h3>
        )}/>
    </Switch>
);

const Article = ({ match }) => (
    <div>
        <h3>{match.params.articleId}</h3>
    </div>
);

export default Articles;
