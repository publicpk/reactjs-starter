import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Articles from './Articles'
import './App.css'

const App = () => (
    <Router>
        <div>
            {/* Navi header */}
            <ul className="router-nav">
                <li><Link to="/">Home |</Link></li>
                <li><Link to="/articles">| Articles |</Link></li>
                <li><Link to="/about">| About</Link></li>
            </ul>
            <hr/>
            {/* Router */}
            <Routes/>
        </div>
    </Router>
);

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/articles" component={Articles} />
        <Route path="/about" component={About} />
        {/* default handler */}
        <Route component={NotFound} />
    </Switch>
);

const Home = () => (<h1>Home</h1>);
const NotFound = () => (<h1>Not Found</h1>);
const About = () => (<h1>About</h1>);

export default App;