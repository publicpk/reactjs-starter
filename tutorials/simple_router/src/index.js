import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import './index.css';

const App = () => (
    <Router>
        <div>
            <ul className="router-nav">
                <li><Link to="/">Home |</Link></li>
                <li><Link to="/articles">| Articles |</Link></li>
                <li><Link to="/about">| About |</Link></li>
                <li><Link to="/old-about">| Redirect to About</Link></li>
            </ul>
            <hr/>
            <Routes/>
        </div>
    </Router>
);

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/articles" component={Articles} />
        <Route path="/about" component={About} />
        <Redirect from="/old-about" to="/about" component={About} />
        {/* default handler */}
        <Route component={NotFound} />
    </Switch>
);

const Home = () => (<h1>Home</h1>);
const NotFound = () => (<h1>Not Found</h1>);
const About = () => (<h1>About</h1>);
const Articles = () => (<h1>Articles</h1>);

const ROOT_NODE = document.getElementById('root');
ReactDOM.render(
    <App />,
    ROOT_NODE);
