/**
 * Created by peterk on 7/6/17.
 */
import React from 'react';
import { Switch, Route, Link, Prompt } from 'react-router-dom';

const MyLink = (props) => (
    <Route path={props.to} children={({ match }) => (
      <div className={ match ? 'active' : '' }>
        <Link { ...props }>{ match ? '> ' : '' }{props.children}</Link>
      </div>
    )}/>
);

const Articles = ({ match }) => (
    <div>
        <h1>Articles</h1>
        <ul>
            <li><MyLink to={`${match.url}/article-01`}> Article 01 </MyLink></li>
            <li><MyLink to={`${match.url}/article-02`}> Article 02 </MyLink></li>
            <li><MyLink to={`${match.url}/article-03`}> Article 03(Leave confirm) </MyLink></li>
        </ul>
        <ArticlesRouters match={match}/>
    </div>
);

const ArticlesRouters = ({ match }) => (
    <Switch>
        <Route path={`${match.url}/:articleId`} component={Article}/>
        <Route exact path={match.url} render = {() => (
           <h3>Please select an article.</h3>
        )}/>
    </Switch>
);

const Article = ({ match }) => (
    <div>
        <Prompt
            when={match.params.articleId === 'article-03'}
            message={location => (
                `Are you sure you want to go to ${location.pathname}`
            )}
        />
        <h3>Article ID: {match.params.articleId}</h3>
    </div>
);

export default Articles;
