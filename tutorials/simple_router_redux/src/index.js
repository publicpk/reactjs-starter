import React from 'react';
import ReactDOM from 'react-dom';
// redux
import { Provider, connect } from "react-redux";
import { createStore, combineReducers } from 'redux'
// router
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

// ================
// reducers
//
const reducer = (state = {count: 0}, action) => {
    switch ( action.type ) {
        default:
            return {...state, count: action.count || 0};
    }
};

const reducers = combineReducers({
    reducer
});

// ================
// actions
//
let count = 0;
const action_noop = () => ({
    type: 'NOOP',
    count: ++count
});

const actions = {
    noop: action_noop
};


// ================
// stores
//
const devtool = window.__REDUX_DEVTOOLS_EXTENSION__;
const devtool_enhancer = devtool && devtool(actions);
const stores = createStore(reducers, devtool_enhancer);


// =================
// containers
//
const Home = ({reducer, noop}) => (
    <div onClick={() => noop()}>
        <h1>Home</h1>
        <p>Click any where to increase the count below</p>
        <p>Count: {reducer.count}</p>
    </div>
);

const mapHomeStateToProps = (state) => ({
    reducer: state.reducer
});
const mapHomeDispatchToProps = (dispatch) => ({
    noop: () => dispatch(actions.noop())
});
const HomeContainer = connect(mapHomeStateToProps, mapHomeDispatchToProps)(Home);

const About = ({reducer}) => (
    <div>
        <h1>About</h1>
        <p>Current count: {reducer.count}</p>
    </div>
);
const AboutContainer = connect((state)=>({reducer:state.reducer}))(About);


// =================
// apps
//
const Routes = () => (
    <Switch>
        <Route path="/" exact component={HomeContainer} />
        <Route path="/about" component={AboutContainer} />
        { /* Finally, catch all unmatched routes */ }
        <Route render={()=><h1>Not Found</h1>} />
    </Switch>
);

const App = () => (
    <Router><div>
        <ul> {/* Navbar */}
            <li style={{display:'inline'}}><Link to="/">| Home |</Link></li>
            <li style={{display:'inline'}}><Link to="/about">| About |</Link></li>
        </ul><hr/>
        {/* Routes */}
        <Routes/>
    </div></Router>
);


// =================
// index.js
//
ReactDOM.render(
    <Provider store={stores}>
        <App />
    </Provider>,
    document.getElementById('root')
);
