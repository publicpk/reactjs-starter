import React from 'react';

import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import {Nav, NavItem, Navbar} from 'react-bootstrap';

import './App.css';

const Home = () => (<h1>Home</h1>);
const About = () => (<h1>About</h1>);
const NotFound = () => (<h1>Not Found</h1>);

const Routes = ({ childProps }) => (
    <Switch>
        <Route path="/" exact component={Home} props={childProps} />
        <Route path="/about" component={About} props={childProps} />

        { /* Finally, catch all unmatched routes */ }
        <Route component={NotFound} />
    </Switch>
);

const App = () => {
    const childProps = {};
    return (
        <Router>
            <div className="App container">
                <Navbar fluid collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to="/">Home</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight>
                            <NavItem><Link to="/about">About</Link></NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Routes childProps={childProps}/>
            </div>
        </Router>
    )};

export default App;
