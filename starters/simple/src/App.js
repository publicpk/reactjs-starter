import React from 'react';

import './App.css';

const Home = () => (<h1>Home</h1>);

const App = () => {
    return (
        <div className="App">
            <Home/>
        </div>
    )};

export default App;
