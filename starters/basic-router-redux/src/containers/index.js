/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import Home from './Home';
import { AboutContainer } from './misc';

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" component={AboutContainer} />
        { /* Finally, catch all unmatched routes */ }
        <Route render={()=><h1>Not Found</h1>} />
    </Switch>
);

const App = () => (
    <Router><div>
        <ul> {/* Navbar */}
            <li style={{display:'inline'}}><Link to="/">| Home |</Link></li>
            <li style={{display:'inline'}}><Link to="/about">| About |</Link></li>
        </ul><hr/>
        {/* Routes */}
        <Routes/>
    </div></Router>
);

export default App;
