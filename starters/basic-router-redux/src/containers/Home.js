/**
 * Created by peterk on 7/11/17.
 */
import React from 'react';
import { connect } from 'react-redux';

import actions from '../actions';

const Home = ({reducer, noop}) => {
    return (
        <div onClick={() => noop()}>
            <h1>Home</h1>
            <p>Click any where to increase the count below</p>
            <p>Count: {reducer.count}</p>
        </div>
    );
};

const mapHomeStateToProps = (state) => ({
    reducer: state.reducer
});
const mapHomeDispatchToProps = (dispatch) => ({
    noop: () => dispatch(actions.action_noop())
});
const HomeContainer = connect(mapHomeStateToProps, mapHomeDispatchToProps)(Home);

export default HomeContainer;
