/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';
import { connect } from 'react-redux';

const About = ({reducer}) => (
    <div>
        <h1>About</h1>
        <p>Current count: {reducer.count}</p>
    </div>
);
const AboutContainer = connect((state)=>({reducer:state.reducer}))(About);

export {
    AboutContainer
};
