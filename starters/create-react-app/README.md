This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Quick Overview
```
npm install -g yarn
npm install -g create-react-app
create-react-app my-app
cd my-app
# start app
yarn start

```

## command line
- yarn start
    Starts the development server.

- yarn build
    Bundles the app into static files for production.

- yarn test
    Starts the test runner.

- yarn eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!
