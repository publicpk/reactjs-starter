# ReactJS Starter #

## Directories
- /starters: various starter templates
- /tutorials: simple tutorials. (currently working on it)
- /workspace.git: ignore it. it's my working directory

## Useful Links
- [User manual of create-react-app](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md)
    - [Code Splitting](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#code-splitting)
- [HTML to JSX Compiler](http://magic.reactjs.net/htmltojsx.htm)
- [React Router](https://reacttraining.com/react-router/web)    
- [default vs named import/export](https://stackoverflow.com/questions/36795819/when-should-i-use-curly-braces-for-es6-import/36796281#36796281)
- [Practical Redux](http://blog.isquaredsoftware.com/series/practical-redux/)
- [React/Redux Links](https://github.com/markerikson/react-redux-links)
